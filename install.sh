#!/bin/bash -e

[ -n "${DISTRO}" ] || exit 1
[ -n "${ROOT_REPO}" ] || exit 1
[ -n "${GIT_NAME}" ] || exit 1
[ -n "${GIT_EMAIL}" ] || exit 1

function install_distro_packages {
	pushd ./packages/${DISTRO}
		if [ 0 -eq `id -u` ]; then
			./install.sh
		else
			sudo -E ./install.sh
		fi
	popd
}

function install_nodejs_packages {
	pushd ./packages/
		if [ 0 -eq `id -u` ]; then
			npm install -g `cat nodejs_packages.txt`
		else
			sudo npm install -g `cat nodejs_packages.txt`
		fi
	popd
}

function install_python_packages {
	pushd ./packages/
		if [ 0 -eq `id -u` ]; then
			find /usr/lib/python3*/ -name "EXTERNALLY-MANAGED" -delete
			python3 -m pip install `cat python_packages.txt`
		else
			sudo find /usr/lib/python3*/ -name "EXTERNALLY-MANAGED" -delete
			sudo python3 -m pip install `cat python_packages.txt`
		fi
	popd
}

function setup_git {
	pushd ${HOME}
		git config --global merge.tool vimdiff 
		git config --global merge.conflictstyle diff3 
		git config --global mergetool.prompt false 
		git config --global user.email "${GIT_EMAIL}" 
		git config --global user.name "${GIT_NAME}"
	popd
}

function setup_1password {
	ARCH="amd64" VERSION=2.30.0 && \
	wget "https://cache.agilebits.com/dist/1P/op2/pkg/v${VERSION}/op_linux_${ARCH}_v${VERSION}.zip" -O /tmp/op.zip && \
	mkdir -p /tmp/op && \
	unzip -d /tmp/op /tmp/op.zip && \
	sudo mv /tmp/op/op /usr/local/bin/ && \
	rm -r /tmp/op.zip /tmp/op && \
	sudo groupadd -f onepassword-cli && \
	sudo chgrp onepassword-cli /usr/local/bin/op && \
	sudo chmod g+s /usr/local/bin/op
}

install_distro_packages
install_nodejs_packages
install_python_packages

for repo in vim_config vifm_config bash_config; do
	git clone ${ROOT_REPO}/${repo}.git
	pushd $repo
		./install.sh
	popd
done

setup_git
setup_1password
