#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive 

apt update
apt install -yq git 

git clone ${ROOT_REPO}/distro_packages -b ${DISTRO} ${DISTRO}
pushd ${DISTRO}
	pushd dev
		apt update 
		apt install -yq `cat packages.txt`
	popd
popd

curl -sL https://deb.nodesource.com/setup_current.x | bash -
apt update
apt install -yq nodejs 

wget https://github.com/Peltoche/lsd/releases/download/0.21.0/lsd_0.21.0_amd64.deb
dpkg -i lsd_0.21.0_amd64.deb
rm -frv lsd_0.21.0_amd64.deb
